package br.com.roove.sippa.adapters;

import java.util.List;

import br.com.roove.sippa.R;
import br.com.roove.sippa.model.Arquivo;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterArquivos extends ArrayAdapter<Arquivo>{
	
	private Context context;
	private LayoutInflater inflater;

	public AdapterArquivos(Context context, int resource, List<Arquivo> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context = context;
		
	}
	
	class ViewHolder {
		
		TextView nome_do_arquivo;
		TextView data_do_arquivo;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		inflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		
		Arquivo arquivo = new Arquivo(); 
		arquivo = getItem(position);
		ViewHolder holder;
		
		if (convertView == null){
			
			convertView = inflater.inflate(R.layout.item_lista_arquivos, parent, false);
			
			holder = new ViewHolder();
			holder.data_do_arquivo = (TextView)convertView.findViewById(R.id.data_do_arquivo);
			holder.nome_do_arquivo = (TextView)convertView.findViewById(R.id.nome_do_arquivo);
			
			holder.data_do_arquivo.setText(arquivo.getData());
			holder.nome_do_arquivo.setText(arquivo.getNome());
			
			convertView.setTag(holder);
			
		}else {
			
			holder = (ViewHolder)convertView.getTag();
			
			holder.data_do_arquivo.setText(arquivo.getData());
			holder.nome_do_arquivo.setText(arquivo.getNome());
		}
		
		return convertView;
	}

}
