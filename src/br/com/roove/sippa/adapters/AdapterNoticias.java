package br.com.roove.sippa.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.model.Frequencia;
import br.com.roove.sippa.model.Noticia;

public class AdapterNoticias extends ArrayAdapter<Noticia>{
	
	Context context;
	LayoutInflater fInflater;

	public AdapterNoticias(Context context, int resource, List<Noticia> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	
	public class ViewHolder {
		TextView data;
		TextView noticia;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		fInflater = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder;
		Noticia noticia = new Noticia();
		noticia = getItem(position);
		
		if (convertView == null){
			convertView = fInflater.inflate(R.layout.simple_item_noticias, parent, false);
			holder = new ViewHolder();
			holder.noticia = (TextView)convertView.findViewById(R.id.noticia);
			holder.data = (TextView)convertView.findViewById(R.id.data_noticia);
			
			
			holder.noticia.setText(noticia.getNoticia());
			holder.data.setText(noticia.getData());
			
			
			convertView.setTag(holder);
		}else{
			
			holder = (ViewHolder)convertView.getTag();
			
			holder.noticia = (TextView)convertView.findViewById(R.id.noticia);
			holder.data = (TextView)convertView.findViewById(R.id.data_noticia);
			
			
			holder.noticia.setText(noticia.getNoticia());
			holder.data.setText(noticia.getData());
			
			
		}
		
		
		return convertView;
	}
	
	
	
	
}
