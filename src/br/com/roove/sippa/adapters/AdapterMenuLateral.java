package br.com.roove.sippa.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.model.OpcoesMenuLateral;

public class AdapterMenuLateral extends ArrayAdapter<OpcoesMenuLateral>{
	

	public AdapterMenuLateral(Context context, int resource,
			List<OpcoesMenuLateral> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	private Context context;
	
	private class ViewHolder {
		ImageView icon;
		TextView op;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder holder = null;
		OpcoesMenuLateral op = new OpcoesMenuLateral();
		op = getItem(position);
		
		LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null){
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_menu_lateral, parent, false);
			holder.icon = (ImageView)convertView.findViewById(R.id.icon_menu_lateral);
			holder.op = (TextView)convertView.findViewById(R.id.opcao_menu_lateral);
			holder.op.setText(op.getOpcao());
			holder.icon.setImageResource(op.getId());
			
			convertView.setTag(holder);
			
		}else {
			
			holder = (ViewHolder)convertView.getTag();
			
			holder.op.setText(op.getOpcao());
			holder.icon.setImageResource(op.getId());
		}
		
		return convertView;
	}
	
	

}
