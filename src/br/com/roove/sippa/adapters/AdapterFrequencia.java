package br.com.roove.sippa.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.model.Frequencia;

public class AdapterFrequencia extends ArrayAdapter<Frequencia>{
	
	Context context;
	LayoutInflater fInflater;
	List<Frequencia> frequencias;

	public AdapterFrequencia(Context context, int resource,
			List<Frequencia> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.frequencias = objects;
	}
	
	public class ViewHolder{
		TextView conteudo;
		TextView data;
		TextView presenca;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		fInflater = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder;
		Frequencia frequencia = new Frequencia();
		frequencia = getItem(position);
		
		if (convertView == null){
			convertView = fInflater.inflate(R.layout.item_lista_frequencia, parent, false);
			holder = new ViewHolder();
			holder.conteudo = (TextView)convertView.findViewById(R.id.conteudo_aula_frequencia);
			holder.data = (TextView)convertView.findViewById(R.id.data_da_aula);
			holder.presenca = (TextView)convertView.findViewById(R.id.presente_ou_nao);
			
			holder.conteudo.setText(frequencia.getConteudo());
			holder.data.setText(frequencia.getData());
			holder.presenca.setText(frequencia.getPresente());
			
			Log.d("conv", "convertView_null");
			
			convertView.setTag(holder);
		}else{
			
			holder = (ViewHolder)convertView.getTag();
			
			
			Log.d("conv", "convertView");
		}
		
		holder.conteudo.setText(frequencia.getConteudo());
		holder.data.setText(frequencia.getData());
		
		holder.presenca.setText(frequencia.getPresente());
		
		
		return convertView;
	}
	
	
	
	

}
