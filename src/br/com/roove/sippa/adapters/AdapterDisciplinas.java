package br.com.roove.sippa.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.model.Disciplina;

public class AdapterDisciplinas extends ArrayAdapter<Disciplina>{
	
	Context context;
	

	public AdapterDisciplinas(Context context, int resource,
			List<Disciplina> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context = context;
	}
	
	public class ViewHolder{
		TextView disciplina;
		TextView professor;
		TextView porcentagemPresenca;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder holder = null;
		Disciplina disciplina = new Disciplina();
		disciplina = getItem(position);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null){
			convertView = inflater.inflate(R.layout.item_lista_disciplinas, parent, false);
			holder = new ViewHolder();
			holder.disciplina = (TextView)convertView.findViewById(R.id.disciplina_lista);
			holder.professor = (TextView)convertView.findViewById(R.id.professor_disciplina_lista);
			holder.porcentagemPresenca = (TextView)convertView.findViewById(R.id.porcentagem_lista_disciplina);
			
			holder.disciplina.setText(disciplina.getNome());
			holder.professor.setText(disciplina.getProfessor());
			holder.porcentagemPresenca.setText(disciplina.getPorcentagem_frequencia() + "%");
			
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
			holder.disciplina = (TextView)convertView.findViewById(R.id.disciplina_lista);
			holder.professor = (TextView)convertView.findViewById(R.id.professor_disciplina_lista);
			holder.professor = (TextView)convertView.findViewById(R.id.porcentagem_lista_disciplina);
			
			holder.disciplina.setText(disciplina.getNome());
			holder.professor.setText(disciplina.getProfessor());
			holder.porcentagemPresenca.setText(disciplina.getPorcentagem_frequencia() + "%");
		}
		
		return convertView;
		
	}
	
	

}
