package br.com.roove.sippa.ui;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import br.com.roove.sippa.R;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Atualizacao extends Activity {
	
	int increment;
	ProgressBar dialog;
	SharedPreferences.Editor prefs_editor;
	SharedPreferences prefs;
	String login;
	String senha;
	ImageView image;
	String session_roove;
	String URL = "http://sippa.roove.com.br";
	LinearLayout layout;
	LinearLayout lil;
	String captcha;
	EditText captcha_text;
	
	public void onClickAtualizar(View view) throws Exception{
		
		captcha = captcha_text.getText().toString();
		
		lil.setVisibility(View.VISIBLE);
		
		layout.setVisibility(LinearLayout.INVISIBLE);
		
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		
		params.put("login", login);
		params.put("senha", senha);
		params.put("captcha", captcha);
		params.put("roove_session_id", session_roove);
		client.setTimeout(180000);
		client.post(URL, params, new JsonHttpResponseHandler(){

			@Override
			public void onFailure(Throwable arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1);
				//pd.dismiss();
				//msgFalha.setText("N�o foi poss�vel acessar o SIPPA, tente novamente!");
				
				lil.setVisibility(View.GONE);
				
				Toast.makeText(getApplicationContext(), "N�o foi poss�vel acessar o SIPPA", Toast.LENGTH_SHORT).show();
				
				AsyncHttpClient client1 = new AsyncHttpClient();
				client1.setTimeout(180000);
				client1.post(URL, null, new JsonHttpResponseHandler(){


					@Override
					public void onSuccess(JSONObject arg0) {
						// TODO Auto-generated method stub
						super.onSuccess(arg0);
						String URL_CAPTCHA;
						try {
							
							URL_CAPTCHA = arg0.getString("captcha");
							session_roove = arg0.getString("roove_session_id");
							UrlImageViewHelper uHelper = new UrlImageViewHelper();
							uHelper.setUrlDrawable(image, URL_CAPTCHA);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				
				layout.setVisibility(LinearLayout.VISIBLE);
			}

			@Override
			public void onSuccess(JSONArray arg0) {
				// TODO Auto-generated method stub
				super.onSuccess(arg0);
				//pd.dismiss();
				try {
					JSONObject jsonObject = arg0.getJSONObject(0);
					String status = jsonObject.getString("cod");
					
					if (!status.equals("success")){
						//msgFalha.setText(jsonObject.getString("msg"));
						
						lil.setVisibility(View.GONE);
						
						Toast.makeText(getApplicationContext(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
						
						AsyncHttpClient client1 = new AsyncHttpClient();
						client1.setTimeout(180000);
						client1.post(URL, null, new JsonHttpResponseHandler(){


							@Override
							public void onSuccess(JSONObject arg0) {
								// TODO Auto-generated method stub
								super.onSuccess(arg0);
								String URL_CAPTCHA;
								try {
									
									URL_CAPTCHA = arg0.getString("captcha");
									session_roove = arg0.getString("roove_session_id");
									UrlImageViewHelper uHelper = new UrlImageViewHelper();
									uHelper.setUrlDrawable(image, URL_CAPTCHA);
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						});
						
						layout.setVisibility(LinearLayout.VISIBLE);
						
					}else{
						
						
						
						String json = arg0.toString();
						
						prefs_editor.putString("json", json).commit();
						
						Intent it = new Intent(getApplicationContext(), ListaDeDisciplinas.class);
						it.putExtra("json", json);
						
						startActivity(it);
						
						finalizar();
						
						lil.setVisibility(View.GONE);
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			@Override
			public void onSuccess(JSONObject arg0) {
				
				super.onSuccess(arg0);
			}

			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				super.onStart();
				//pd = ProgressDialog.show(context, null, "Entrando...")
				
			}
			
			
			
		});
		
	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_atualizacao);
		
		dialog = (ProgressBar)findViewById(R.id.pbHeaderProgress2);
		
		lil = (LinearLayout)findViewById(R.id.linlaHeaderProgress2);
		
		image = (ImageView)findViewById(R.id.imageViewCaptchaAtualizacao);
		layout = (LinearLayout)findViewById(R.id.linearLayoutLoginAtualizacao);
		captcha_text = (EditText)findViewById(R.id.editTextCaptchaAtualizao);

		prefs = getSharedPreferences("SIPPA_PREFS", 0);
		prefs_editor = prefs.edit();
		login = prefs.getString("login", "");
		senha = prefs.getString("senha", "");
		
		AsyncHttpClient client1 = new AsyncHttpClient();
		client1.setTimeout(180000);
		client1.post(URL, null, new JsonHttpResponseHandler(){


			@Override
			public void onSuccess(JSONObject arg0) {
				// TODO Auto-generated method stub
				super.onSuccess(arg0);
				String URL_CAPTCHA;
				try {
					
					URL_CAPTCHA = arg0.getString("captcha");
					session_roove = arg0.getString("roove_session_id");
					UrlImageViewHelper uHelper = new UrlImageViewHelper();
					uHelper.setUrlDrawable(image, URL_CAPTCHA);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
	}
	
	public void finalizar(){
		this.finish();
	}
	
public void updatePB(){
		
		increment = 1;
		dialog.setProgress(0);
		dialog.setMax(100);
		
		
		Thread background = new Thread (new Runnable() {
	           public void run() {
	               try {
	                   // enter the code to be run while displaying the progressbar.
	                   //
	                   // This example is just going to increment the progress bar:
	                   // So keep running until the progress value reaches maximum value
	                   while (dialog.getProgress()<= dialog.getMax()) {
	                       // wait 500ms between each update
	                       Thread.sleep(500);
	 
	                       // active the update handler
	                       progressHandler.sendMessage(progressHandler.obtainMessage());
	                   }
	               } catch (java.lang.InterruptedException e) {
	                   // if something fails do something smart
	               }
	           }
	        });
	         
	        // start the background thread
	        background.start();
	 
	    }
	     
	    // handler for the background updating
	    Handler progressHandler = new Handler() {
	        public void handleMessage(Message msg) {
	            dialog.incrementProgressBy(increment);
	        }
	    };
	
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (View.VISIBLE == lil.getVisibility()){
			
		}else{
		super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.atualizacao, menu);
		return true;
	}

}
