package br.com.roove.sippa.ui;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.model.Avaliacao;
import br.com.roove.sippa.model.Disciplina;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class FragmentSegundaChamada extends Fragment{
	
	Spinner avaliacoes;
	EditText justificativa;
	Button enviar;
	ArrayAdapter<String> adAvaliacoes;
	ArrayList<Avaliacao> avaliacoes_array;
	ArrayList<String> avaliacs;
	String avaliacao_selecionada;
	String URL = "";
	ProgressDialog pd;
	TextView disciplina_text;
	
	public ArrayList<String> fillArrayString(ArrayList<Avaliacao> avaliacoes){
		ArrayList<String> avaliacs = new ArrayList<String>();
		
		for (int i = 0 ; i < (avaliacoes.size() - 1) ; i++){
			String aux = avaliacoes.get(i).getNome();
			String avaliacao = aux.substring(0, Math.min(aux.length(), 3));
			avaliacs.add(avaliacao);
			
		}
		
		return avaliacs;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		
		View v = inflater.inflate(R.layout.fragment_segunda_chamada, container, false);
		
		final Disciplina disciplina = (Disciplina)getArguments().getSerializable("disciplina");
		avaliacs = new ArrayList<String>();
		avaliacs = fillArrayString(disciplina.getAvaliacoes());
		adAvaliacoes = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, avaliacs);
		//adAvaliacoes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		disciplina_text = (TextView)v.findViewById(R.id.disciplina_segunda_chamada);
		disciplina_text.setText(disciplina.getNome());
		
		avaliacoes = (Spinner)v.findViewById(R.id.spinnerAvaliacoes);
		avaliacoes.setAdapter(adAvaliacoes);
		
		justificativa = (EditText)v.findViewById(R.id.editText_justificativa_segunda_chamada);
		enviar = (Button)v.findViewById(R.id.button_enviar_solicitacao);
		enviar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				avaliacao_selecionada = avaliacoes.getSelectedItem().toString();
				String just = justificativa.getText().toString();
				String codigo = disciplina.getCodigo();
				
				RequestParams params = new RequestParams();
				params.put("avaliacao", avaliacao_selecionada);
				params.put("justificativa", just);
				params.put("codigo", codigo);
				
				AsyncHttpClient client = new AsyncHttpClient();
				
				client.get(URL, params, new AsyncHttpResponseHandler(){

					@Override
					public void onFailure(Throwable arg0, String arg1) {
						// TODO Auto-generated method stub
						super.onFailure(arg0, arg1);
						pd.dismiss();
					}

					@Override
					public void onStart() {
						// TODO Auto-generated method stub
						super.onStart();
						pd = ProgressDialog.show(getActivity(), null, "Enviando solicitação...");
					}

					@Override
					public void onSuccess(int arg0, String arg1) {
						// TODO Auto-generated method stub
						super.onSuccess(arg0, arg1);
						pd.dismiss();
					}

				});
				
			}
		});
		
		return v;
	}

	

}
