package br.com.roove.sippa.ui;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.roove.sippa.R;
import br.com.roove.sippa.util.AESUtil;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class LoginActivity extends Activity {
	
	ProgressBar dialog;
	int increment;
	LinearLayout lil;
	SharedPreferences prefs;
	SharedPreferences.Editor prefs_editor;
	LinearLayout layout;
	ScrollView sc;
	Context context;
	ImageView captcha_image;
	TextView msgFalha;
	EditText senha_text;
	EditText login_text;
	EditText captcha_text;
	Button entrar;
	String senha;
	String login;
	String captcha;
	String session_roove;
	String URL = "http://sippa.roove.com.br";
	String URL_GET_CAPTCHA = "";
	ProgressDialog pd;
	String senha_cript = "";
	
	public void onClickEntrar (View view) throws Exception{
		senha = senha_text.getText().toString();
		login = login_text.getText().toString();
		captcha = captcha_text.getText().toString();
		
		layout.setVisibility(View.GONE);
		sc.setVisibility(View.GONE);
		
		lil.setVisibility(View.VISIBLE);
		updatePB();
		
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		senha_cript = AESUtil.encrypt(senha);
		System.out.println(senha_cript);
		
		params.put("login", login);
		params.put("senha", senha_cript);
		params.put("captcha", captcha);
		params.put("roove_session_id", session_roove);
		client.setTimeout(180000);
		client.post(URL, params, new JsonHttpResponseHandler(){
			
			

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1);
				
				msgFalha.setText("N�o foi poss�vel acessar o SIPPA, tente novamente!");
				
				lil.setVisibility(View.GONE);
				
				AsyncHttpClient client1 = new AsyncHttpClient();
				client1.setTimeout(180000);
				client1.post(URL, null, new JsonHttpResponseHandler(){


					@Override
					public void onSuccess(JSONObject arg0) {
						// TODO Auto-generated method stub
						super.onSuccess(arg0);
						String URL_CAPTCHA;
						try {
							
							URL_CAPTCHA = arg0.getString("captcha");
							session_roove = arg0.getString("roove_session_id");
							Toast.makeText(getApplicationContext(), session_roove, Toast.LENGTH_SHORT).show();
							UrlImageViewHelper uHelper = new UrlImageViewHelper();
							uHelper.setUrlDrawable(captcha_image, URL_CAPTCHA);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				
				layout.setVisibility(LinearLayout.VISIBLE);
				sc.setVisibility(View.VISIBLE);
				
			}

			@Override
			public void onFailure(Throwable arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1);
				//pd.dismiss();
				msgFalha.setText("N�o foi poss�vel acessar o SIPPA, tente novamente!");
				
				lil.setVisibility(View.GONE);
				
				AsyncHttpClient client1 = new AsyncHttpClient();
				client1.setTimeout(180000);
				client1.post(URL, null, new JsonHttpResponseHandler(){


					@Override
					public void onSuccess(JSONObject arg0) {
						// TODO Auto-generated method stub
						super.onSuccess(arg0);
						String URL_CAPTCHA;
						try {
							
							URL_CAPTCHA = arg0.getString("captcha");
							session_roove = arg0.getString("roove_session_id");
							Toast.makeText(getApplicationContext(), session_roove, Toast.LENGTH_SHORT).show();
							UrlImageViewHelper uHelper = new UrlImageViewHelper();
							uHelper.setUrlDrawable(captcha_image, URL_CAPTCHA);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
				
				layout.setVisibility(LinearLayout.VISIBLE);
				sc.setVisibility(View.VISIBLE);
			}

			@Override
			public void onSuccess(JSONArray arg0) {
				// TODO Auto-generated method stub
				super.onSuccess(arg0);
				//pd.dismiss();
				try {
					JSONObject jsonObject = arg0.getJSONObject(0);
					String status = jsonObject.getString("cod");
					
					if (!status.equals("success")){
						
						lil.setVisibility(View.GONE);
						
						msgFalha.setText(jsonObject.getString("msg"));
						
						AsyncHttpClient client1 = new AsyncHttpClient();
						client1.setTimeout(180000);
						client1.post(URL, null, new JsonHttpResponseHandler(){


							@Override
							public void onSuccess(JSONObject arg0) {
								// TODO Auto-generated method stub
								super.onSuccess(arg0);
								String URL_CAPTCHA;
								try {
									
									URL_CAPTCHA = arg0.getString("captcha");
									session_roove = arg0.getString("roove_session_id");
									UrlImageViewHelper uHelper = new UrlImageViewHelper();
									uHelper.setUrlDrawable(captcha_image, URL_CAPTCHA);
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						});
						
						layout.setVisibility(LinearLayout.VISIBLE);
						sc.setVisibility(View.VISIBLE);
						
					}else{
						
						prefs_editor.putString("login", login).commit();
						prefs_editor.putString("senha", senha_cript).commit();
						
						String json = arg0.toString();
						
						System.out.println(json);
						
						prefs_editor.putString("json", json).commit();
						
						Intent it = new Intent(context, ListaDeDisciplinas.class);
						it.putExtra("json", json);
						
						startActivity(it);
						
						finalizar();
						
						lil.setVisibility(View.GONE);
						
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			@Override
			public void onSuccess(JSONObject arg0) {
				
				super.onSuccess(arg0);
			}

			@Override
			public void onStart() {
				// TODO Auto-generated method stub
				super.onStart();
				//pd = ProgressDialog.show(context, null, "Entrando...");
			}
			
			
			
		});
	}
	
	public void updatePB(){
		
		increment = 1;
		dialog.setProgress(0);
		dialog.setMax(100);
		
		
		Thread background = new Thread (new Runnable() {
	           public void run() {
	               try {
	                   // enter the code to be run while displaying the progressbar.
	                   //
	                   // This example is just going to increment the progress bar:
	                   // So keep running until the progress value reaches maximum value
	                   while (dialog.getProgress()<= dialog.getMax()) {
	                       // wait 500ms between each update
	                       Thread.sleep(500);
	 
	                       // active the update handler
	                       progressHandler.sendMessage(progressHandler.obtainMessage());
	                   }
	               } catch (java.lang.InterruptedException e) {
	                   // if something fails do something smart
	               }
	           }
	        });
	         
	        // start the background thread
	        background.start();
	 
	    }
	     
	    // handler for the background updating
	    Handler progressHandler = new Handler() {
	        public void handleMessage(Message msg) {
	            dialog.incrementProgressBy(increment);
	        }
	    };
		
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (View.VISIBLE == lil.getVisibility()){
			
		}else{
		super.onBackPressed();
		}
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.login_activity);
		
		dialog = (ProgressBar)findViewById(R.id.pbHeaderProgress);
		
		sc = (ScrollView)findViewById(R.id.scrollLogin);
		lil = (LinearLayout)findViewById(R.id.linlaHeaderProgress);
		
		prefs = getSharedPreferences("SIPPA_PREFS", 0);
		prefs_editor = getSharedPreferences("SIPPA_PREFS", 0).edit();
		
		String login = prefs.getString("login", "");
		String senha = prefs.getString("senha", "");
		
		if (!login.equals("") && !senha.equals("")) {
			
			String json = prefs.getString("json", "");
			
			if (!json.equals("")){

			Intent it = new Intent(this, ListaDeDisciplinas.class);
			it.putExtra("json", json);
			startActivity(it);
			
			}
		}
		
		context = getApplicationContext();
		layout = (LinearLayout)findViewById(R.id.linearLayoutLogin);
		captcha_image = (ImageView)findViewById(R.id.imageViewCaptcha);
		senha_text = (EditText)findViewById(R.id.editTextSenha);
		login_text = (EditText)findViewById(R.id.editTextLogin);
		captcha_text = (EditText)findViewById(R.id.editTextCaptcha);
		msgFalha = (TextView)findViewById(R.id.respostaLogin);
		
		entrar = (Button)findViewById(R.id.button1);
		
		AsyncHttpClient client1 = new AsyncHttpClient();
		client1.setTimeout(180000);
		client1.post(URL, null, new JsonHttpResponseHandler(){


			@Override
			public void onSuccess(JSONObject arg0) {
				// TODO Auto-generated method stub
				super.onSuccess(arg0);
				String URL_CAPTCHA;
				try {
					
					URL_CAPTCHA = arg0.getString("captcha");
					session_roove = arg0.getString("roove_session_id");
					UrlImageViewHelper uHelper = new UrlImageViewHelper();
					uHelper.setUrlDrawable(captcha_image, URL_CAPTCHA);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	
	public void finalizar(){
		this.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

}
