package br.com.roove.sippa.ui;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.adapters.AdapterDisciplinas;
import br.com.roove.sippa.model.Arquivo;
import br.com.roove.sippa.model.Avaliacao;
import br.com.roove.sippa.model.Disciplina;
import br.com.roove.sippa.model.Frequencia;
import br.com.roove.sippa.model.Noticia;

public class ListaDeDisciplinas extends ListActivity{
	
	ListView list;
	Disciplina disciplina;
	AdapterDisciplinas aDisciplinas;
	ArrayList<Disciplina> disciplinas;
	JSONArray jsonArray;
	SharedPreferences prefs;
	SharedPreferences.Editor prefs_editor;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.lista_de_disciplinas);
		
		prefs_editor = getSharedPreferences("SIPPA_PREFS", 0).edit();
		
		prefs = getSharedPreferences("SIPPA_PREFS", 0);
		
		Intent it = getIntent();
		String json = it.getStringExtra("json");
		System.out.println(json);
		try {
			jsonArray = new JSONArray(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		disciplina = new Disciplina();
		disciplinas = new ArrayList<Disciplina>();
		disciplinas = fillDisciplina(jsonArray);
		aDisciplinas = new AdapterDisciplinas(getApplicationContext(), R.layout.item_lista_disciplinas, disciplinas);
		list = (ListView)findViewById(android.R.id.list);
		list.setAdapter(aDisciplinas);
	}
	
	public ArrayList<Disciplina> fillDisciplina(JSONArray jsonArray){
		final ArrayList<Disciplina> disciplinas = new ArrayList<Disciplina>();
				
				JSONArray jsonDisciplinas = new JSONArray();
				jsonDisciplinas = jsonArray;
				
				for (int i = 1; i < jsonDisciplinas.length();i++){
					Disciplina disciplina = new Disciplina();
					ArrayList<Avaliacao> avaliacoes = new ArrayList<Avaliacao>();
					ArrayList<Noticia> noticias = new ArrayList<Noticia>();
					ArrayList<Arquivo> arquivos = new ArrayList<Arquivo>();
					ArrayList<Frequencia> frequencias = new ArrayList<Frequencia>();
					
					JSONObject jsonDisciplina = new JSONObject();
					
					try {
						jsonDisciplina = jsonDisciplinas.getJSONObject(i);
						
						JSONArray jsonAvaliacoes = new JSONArray();
						jsonAvaliacoes = jsonDisciplina.getJSONArray("avaliacoes");
						
						JSONArray jsonNoticias = new JSONArray();
						jsonNoticias = jsonDisciplina.getJSONArray("noticias");
						
//						JSONArray jsonArquivos = new JSONArray();
//						jsonArquivos = jsonDisciplina.getJSONArray("arquivos");
						
						JSONArray jsonFrequencias = new JSONArray();
						jsonFrequencias = jsonDisciplina.getJSONArray("aulas");
						
						disciplina.setNome(jsonDisciplina.getString("disciplina"));
						disciplina.setProfessor(jsonDisciplina.getString("professor"));
						disciplina.setEmailProfessor(jsonDisciplina.getString("email"));
						disciplina.setFrequencia_resumo(jsonDisciplina.getString("frequencia_resumo"));
						disciplina.setPorcentagem_frequencia(jsonDisciplina.getString("frequencia"));
						disciplina.setCodigo(jsonDisciplina.getString("codigo"));
						String media = jsonAvaliacoes.getJSONObject(jsonAvaliacoes.length()-1).getString("nota");
						if (media != null && !media.equals("null")){
						disciplina.setMedia(media);
						}else{
							disciplina.setMedia("");
						}
//						for (int j=0;j<jsonArquivos.length();j++){
//							
//							Arquivo arquivo = new Arquivo();
//							JSONObject objArquivo = new JSONObject();
//							objArquivo = jsonArquivos.getJSONObject(j);
//							arquivo.setNome(objArquivo.getString("nome_arquivo"));
//							arquivo.setData(objArquivo.getString("data_arquivo"));
//							arquivo.setURL(objArquivo.getString("url_arquivo"));
//							
//							arquivos.add(arquivo);
//						}
						
						for (int j = 0;j < jsonAvaliacoes.length() - 1; j++){
							Avaliacao avaliacao = new Avaliacao();
							JSONObject objAvaliacao = new JSONObject();
							objAvaliacao = jsonAvaliacoes.getJSONObject(j);
							avaliacao.setNome(objAvaliacao.getString("titulo"));
							avaliacao.setNota("");
							if (objAvaliacao.getString("nota") != null && !objAvaliacao.getString("nota").equals("null")){
							avaliacao.setNota(objAvaliacao.getString("nota"));
							}
							avaliacoes.add(avaliacao);
						}
						
						for (int j = 0; j < jsonNoticias.length(); j++){
							Noticia noticia = new Noticia();
							JSONObject objNoticia = new JSONObject();
							objNoticia = jsonNoticias.getJSONObject(j);
							noticia.setData(objNoticia.getString("data"));
							noticia.setNoticia(objNoticia.getString("noticia"));
							
							noticias.add(noticia);
						}
						
						for (int j = 0; j < jsonFrequencias.length(); j++){
							Frequencia frequencia = new Frequencia();
							JSONObject objFrequencia = new JSONObject();
							objFrequencia = jsonFrequencias.getJSONObject(j);
							frequencia.setConteudo(objFrequencia.getString("titulo"));
							frequencia.setData(objFrequencia.getString("data"));
							frequencia.setPresente(objFrequencia.getString("presenca"));
							
							frequencias.add(frequencia);
						}
						
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					disciplina.setArquivos(arquivos);
					disciplina.setAvaliacoes(avaliacoes);
					disciplina.setFrequencia(frequencias);
					disciplina.setNoticias(noticias);
					
					disciplinas.add(disciplina);
					
				}	
		
		
		return disciplinas;
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lista_de_disciplinas, menu);
		return true;
	}
	
	

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		// TODO Auto-generated method stub
		
		switch (item.getItemId()) {
		case R.id.action_atualizar:
			
			new AlertDialog.Builder(this)
		    .setTitle("Atualizar")
		    .setMessage
		    ("Deseja atualizar seus dados?")
		    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // continue with delete
					
		        	Intent intent = new Intent(getApplicationContext(), Atualizacao.class);
					startActivity(intent);
					finish();
		        }
		     })
		    .setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // do nothing
		        }
		     })
		     .show();

			break;
			
		case R.id.action_sair:
			
			new AlertDialog.Builder(this)
		    .setTitle("Sair")
		    .setMessage
		    ("Voc� deseja realmente sair?")
		    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // continue with delete
		        	prefs_editor.remove("login").commit();
					prefs_editor.remove("senha").commit();
					prefs_editor.remove("json").commit();
					
					Intent intent2 = new Intent(getApplicationContext(), LoginActivity.class);
					startActivity(intent2);
					finish();
		        }
		     })
		    .setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // do nothing
		        }
		     })
		     .show();
			
			break;

		default:
			break;
		}
		
		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Disciplina disciplina = disciplinas.get(position);
		Intent it = new Intent(getApplicationContext(), DisciplinaAberta.class);
		it.putExtra("Disciplina", disciplina);
		startActivity(it);
		
	}

//	@Override
//	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//		// TODO Auto-generated method stub
//		Disciplina disciplina = disciplinas.get(arg2);
//		Intent it = new Intent(this, DisciplinaAberta.class);
//		it.putExtra("Disciplina", disciplina);
//		startActivity(it);
//		
//	}

}
