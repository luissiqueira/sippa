package br.com.roove.sippa.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.model.Avaliacao;
import br.com.roove.sippa.model.Disciplina;

public class FragmentAvaliacoes extends ListFragment {
	
	ListView list;
	TextView media;
	ArrayAdapter<Avaliacao> avAdapter;
	ArrayList<Avaliacao> avaliacoes;
	TextView text_disciplina;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View v = inflater.inflate(R.layout.fragment_avaliacoes, container, false);
		media = (TextView)v.findViewById(R.id.media);
		Disciplina disciplina = new Disciplina();
		disciplina = (Disciplina)getArguments().getSerializable("disciplina");
		media.setText("M�dia - " + disciplina.getMedia());
		media.setTextSize(18);
		text_disciplina = (TextView)v.findViewById(R.id.disciplina_avaliacoes);
		text_disciplina.setText(disciplina.getNome());
		list = (ListView)v.findViewById(android.R.id.list);
		avaliacoes = new ArrayList<Avaliacao>();
		avaliacoes = disciplina.getAvaliacoes();
		

		return v;
	}
	
	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		avAdapter = new ArrayAdapter<Avaliacao>(getActivity(), android.R.layout.simple_list_item_1, avaliacoes);
		list.setAdapter(avAdapter);
		
		
	}
	
	

}
