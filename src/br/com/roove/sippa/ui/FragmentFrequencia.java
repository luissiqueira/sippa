package br.com.roove.sippa.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.adapters.AdapterFrequencia;
import br.com.roove.sippa.model.Disciplina;
import br.com.roove.sippa.model.Frequencia;

public class FragmentFrequencia extends ListFragment{
	
	LayoutInflater mInflater;
	ListView list;
	AdapterFrequencia frequencia;
	//ArrayAdapter<Frequencia> frequencias;
	ArrayList<Frequencia> frequencias;
	TextView infos_frequencia;
	TextView disciplina_text;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View v = inflater.inflate(R.layout.fragmente_frequencia, container, false);
		
		infos_frequencia = (TextView)v.findViewById(R.id.infos_frequencia);
		disciplina_text = (TextView)v.findViewById(R.id.disciplina_frequencia);
		
		Disciplina disciplina = new Disciplina();
		disciplina = (Disciplina)getArguments().getSerializable("disciplina");
		
		String frequencia_resumo = disciplina.getFrequencia_resumo();
		String frequencia1[] = frequencia_resumo.split(";");
		
		infos_frequencia.setText(frequencia1[0] + "\n" + frequencia1[1] + "\n" + frequencia1[2]);
		disciplina_text.setText(disciplina.getNome());
		
		frequencias = disciplina.getFrequencia();
		frequencia = new AdapterFrequencia(getActivity(), R.layout.item_lista_frequencia, frequencias);
		list = (ListView)v.findViewById(android.R.id.list);
		list.setAdapter(frequencia);

		return v;
	}
	
	

}
