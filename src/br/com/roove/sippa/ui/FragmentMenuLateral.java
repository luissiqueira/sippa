package br.com.roove.sippa.ui;

import java.io.File;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import br.com.roove.sippa.R;
import br.com.roove.sippa.adapters.AdapterMenuLateral;
import br.com.roove.sippa.model.OpcoesMenuLateral;

public class FragmentMenuLateral extends ListFragment{
	
	ObjectOutput out;
	ObjectInput inp;
	File file;
	
	SharedPreferences prefs;
	SharedPreferences.Editor prefs_editor;
	ArrayList<OpcoesMenuLateral> opcoes;
	ListView list;
	AdapterMenuLateral mlAdapter;
	
	public ArrayList<OpcoesMenuLateral> preencherArrayDeOpcao () {
		ArrayList<OpcoesMenuLateral> ops = new ArrayList<OpcoesMenuLateral>();
		
		OpcoesMenuLateral op1 = new OpcoesMenuLateral();
		op1.setOpcao("Disciplinas");
		op1.setId(R.drawable.disciplinas_m);
		
		OpcoesMenuLateral op2 = new OpcoesMenuLateral();
		op2.setOpcao("Not�cias");
		op2.setId(R.drawable.noticias_m);
		
		OpcoesMenuLateral op3 = new OpcoesMenuLateral();
		op3.setOpcao("Avalia��es");
		op3.setId(R.drawable.avaliacoes_m);
		
		OpcoesMenuLateral op4 = new OpcoesMenuLateral();
		op4.setOpcao("Frequ�ncia");
		op4.setId(R.drawable.frequencia_m);
		
		OpcoesMenuLateral op5 = new OpcoesMenuLateral();
		op5.setOpcao("Arquivos");
		op5.setId(R.drawable.arquivos_m);
		
		OpcoesMenuLateral op6 = new OpcoesMenuLateral();
		op5.setOpcao("Solicitar 2� Chamada");
		op5.setId(R.drawable.segunda_chamada_m);
		
		
		OpcoesMenuLateral op7 = new OpcoesMenuLateral();
		op6.setOpcao("Atualizar");
		op6.setId(R.drawable.update);
		
		OpcoesMenuLateral op8 = new OpcoesMenuLateral();
		op7.setOpcao("Sair");
		op7.setId(R.drawable.sair);
		
		ops.add(op1);
		ops.add(op2);
		ops.add(op3);
		ops.add(op4);
		ops.add(op5);
		ops.add(op6);
		ops.add(op7);
		ops.add(op8);
		
		return ops;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		prefs = getActivity().getSharedPreferences("SIPPA_PREFS", 0);
		
		prefs_editor = getActivity().getSharedPreferences("SIPPA_PREFS", 0).edit();
		
		View viewRoot = inflater.inflate(R.layout.menu_lateral, container, false);
		list = (ListView)viewRoot.findViewById(android.R.id.list);
		return viewRoot;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		opcoes = new ArrayList<OpcoesMenuLateral>();
		opcoes = preencherArrayDeOpcao();
		mlAdapter = new AdapterMenuLateral(getActivity(), R.layout.item_menu_lateral, opcoes);
		list.setAdapter(mlAdapter);
		
		super.onActivityCreated(savedInstanceState);
	}
	
	

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
		switch (position) {
		case 0:
			
			getActivity().finish();
			
//			String json = prefs.getString("json", "");
//			
//			if (!json.equals("")){
//			
//			Intent it = new Intent(getActivity(), ListaDeDisciplinas.class);
//			it.putExtra("json", json);
//			startActivity(it);
//			
//			getActivity().finish();
//			
//			}else{
//				
//				Toast.makeText(getActivity(), "Sem dados, por favor atualize o aplicativo", Toast.LENGTH_SHORT).show();
//				
//			}
			break;
			
		case 1:
			if (getActivity() instanceof DisciplinaAberta){
				DisciplinaAberta ma = (DisciplinaAberta) getActivity();
				ma.switchFragment(new FragmentNoticias(), "Noticias");		
			}
			
			break;
		
		case 2:
			if (getActivity() instanceof DisciplinaAberta){
				DisciplinaAberta ma = (DisciplinaAberta) getActivity();
				ma.switchFragment(new FragmentAvaliacoes(), "Avaliacoes");		
			}
			break;
			
		case 3:
			if (getActivity() instanceof DisciplinaAberta){
				DisciplinaAberta ma = (DisciplinaAberta) getActivity();
				ma.switchFragment(new FragmentFrequencia(), "Frequencia");		
			}
			break;
			
		case 4:
			
			if (getActivity() instanceof DisciplinaAberta){
				DisciplinaAberta ma = (DisciplinaAberta) getActivity();
				ma.switchFragment(new FragmentArquivos(), "Frequencia");		
			}
			break;
			
		case 5:
			
			//Toast.makeText(getActivity(), "Funcionalidade em implementa��o...", Toast.LENGTH_SHORT).show();
						
						
			if (getActivity() instanceof DisciplinaAberta){
				DisciplinaAberta ma = (DisciplinaAberta) getActivity();
				ma.switchFragment(new FragmentSegundaChamada(), "SegundaChamada");
				}
			
			break;
			
		case 6:
			
			Intent intent = new Intent(getActivity(), Atualizacao.class);
			startActivity(intent);
			
			break;
			
		case 7:
			
			new AlertDialog.Builder(getActivity())
		    .setTitle("Sair")
		    .setMessage("Voc� deseja realmente sair?")
		    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // continue with delete
		        	prefs_editor.remove("login").commit();
					prefs_editor.remove("senha").commit();
					prefs_editor.remove("json").commit();
					
					Intent intent2 = new Intent(getActivity(), LoginActivity.class);
					startActivity(intent2);
		        }
		     })
		    .setNegativeButton("N�o", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // do nothing
		        }
		     })
		     .show();
			
			break;
			
		default:
			break;
		}
	}

//	@Override
//	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//		// TODO Auto-generated method stub
//		
//		switch (arg2) {
//		case 0:
//			Intent it = new Intent(getActivity(), ListaDeDisciplinas.class);
//			startActivity(it);
//			break;
//			
//		case 1:
//			if (getActivity() instanceof DisciplinaAberta){
//				DisciplinaAberta ma = (DisciplinaAberta) getActivity();
//				ma.switchFragment(new FragmentAvaliacoes(), "Avaliacoes");		
//			}
//		
//		case 2:
//			if (getActivity() instanceof DisciplinaAberta){
//				DisciplinaAberta ma = (DisciplinaAberta) getActivity();
//				ma.switchFragment(new FragmentFrequencia(), "Frequencia");		
//			}
//			break;
//			
//		case 3:
//			if (getActivity() instanceof DisciplinaAberta){
//				DisciplinaAberta ma = (DisciplinaAberta) getActivity();
//				ma.switchFragment(new FragmentSegundaChamada(), "SegundaChamada");		
//			}
//			break;
//		default:
//			break;
//		}
//		
//	}
	
	
	
	
	

}
