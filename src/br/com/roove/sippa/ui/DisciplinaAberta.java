package br.com.roove.sippa.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.model.Disciplina;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class DisciplinaAberta extends FragmentActivity {
	
	
	SharedPreferences.Editor prefs_editor;
	String name_frag;
	SharedPreferences prefs;
	ListView list;
	SlidingMenu menu;
	private Fragment mFrag;
	Bundle args;
	TextView nome_email_professor;
	
	public void onClickInfo (View view){
		Intent it = new Intent(this, Roove.class);
		startActivity(it);
	}
	
	public void onClickLayoutInfo (View view) {
		Intent it = new Intent(this, Roove.class);
		startActivity(it);
	}
	
	public void onClickSlideButton(View view){
		menu.showMenu();
	}
	
	public void onClickSlide(View view){
		menu.showMenu();
	}

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_disciplina_aberta);
		Intent it = getIntent();
		Disciplina disciplina = (Disciplina)it.getSerializableExtra("Disciplina");
		
		
		nome_email_professor = (TextView)findViewById(R.id.nome_e_email_professor_bottom);
		
		nome_email_professor.setText(disciplina.getProfessor() + " - " + disciplina.getEmailProfessor());
		
		list = (ListView)findViewById(android.R.id.list);
		
		DisciplinaAberta da = (DisciplinaAberta)this;
		
		args = new Bundle();
		args.putSerializable("disciplina", disciplina);
		
		FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
		mFrag = new FragmentNoticias();
		mFrag.setArguments(args);
		t.add(R.id.fragment_disciplina_aberta, mFrag);
		t.commit();
		
		name_frag = "Noticias";
		
		menu = new SlidingMenu(this);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		menu.setMode(SlidingMenu.LEFT);
        menu.setShadowWidthRes(R.dimen.shadow_widht);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.sliding_menu_offset);
        menu.setFadeDegree(0.70f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        menu.setMenu(R.layout.frame_menu_lateral);
        
        getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.fragment_menu_lateral, new FragmentMenuLateral())
		.commit();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (menu.isMenuShowing()){
			menu.showContent();
		}else{
		
		super.onBackPressed();
		}
	}
	
	public void switchFragment (Fragment f, String tag){	
		menu.showContent();
		
		//String name = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount()-1).getName();
		
		if (!name_frag.equals(tag)){
			
			name_frag = tag;
			
			f.setArguments(args);
			getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.fragment_disciplina_aberta, f)
			.commit();
		
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.disciplina_aberta, menu);
		return true;
	}

}
