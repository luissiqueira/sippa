package br.com.roove.sippa.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.model.Arquivo;
import br.com.roove.sippa.model.Disciplina;

public class FragmentArquivos extends ListFragment{
	
	ListView list;
	ArrayAdapter<Arquivo> arqAdapter;
	ArrayList<Arquivo> arquivos;
	TextView disciplina;
	Bundle b;
	Disciplina disciplina2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View v = inflater.inflate(R.layout.fragment_arquivos, container, false);
		b = getArguments();
		disciplina2 = (Disciplina)b.getSerializable("disciplina");
		disciplina = (TextView)v.findViewById(R.id.disciplina_arquivos);
		list = (ListView)v.findViewById(android.R.id.list);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		arquivos = new ArrayList<Arquivo>();
		arquivos = disciplina2.getArquivos();
		arqAdapter = new ArrayAdapter<Arquivo>(getActivity(), R.layout.item_lista_arquivos, arquivos);
		list.setAdapter(arqAdapter);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
		String URL_to_download = arquivos.get(position).getURL();
		
	}	

}
