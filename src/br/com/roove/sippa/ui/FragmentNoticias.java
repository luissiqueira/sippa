package br.com.roove.sippa.ui;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import br.com.roove.sippa.R;
import br.com.roove.sippa.adapters.AdapterNoticias;
import br.com.roove.sippa.model.Disciplina;
import br.com.roove.sippa.model.Noticia;

public class FragmentNoticias extends ListFragment{
	
	ListView list;
	ArrayList<Noticia> noticias;
	AdapterNoticias nAdapter;
	//ArrayAdapter<Noticia> nAdapter;
	TextView text_disciplina;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View v = inflater.inflate(R.layout.fragment_noticias, container, false);
		Disciplina disciplina = new Disciplina();
		disciplina = (Disciplina)getArguments().getSerializable("disciplina");
		noticias = new ArrayList<Noticia>();
		noticias = disciplina.getNoticias();
		text_disciplina = (TextView)v.findViewById(R.id.disciplina_noticias);
		text_disciplina.setText(disciplina.getNome());
		list = (ListView)v.findViewById(android.R.id.list);
		nAdapter = new AdapterNoticias(getActivity(), R.layout.simple_item_noticias, noticias);
		list.setAdapter(nAdapter);
		
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		
	}
	
	
	

}
