package br.com.roove.sippa.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.Window;
import br.com.roove.sippa.R;

public class Splash extends Activity {

	private final int SPLASH_DISPLAY_LENGHT = 3000;
	SharedPreferences prefs;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);

//		if (android.os.Build.VERSION.SDK_INT > 11){
//			ActionBar ac = getActionBar();
//			//ac.setDisplayShowTitleEnabled(false);
//			ac.hide();
//		}
		
		prefs = getSharedPreferences("SIPPA_PREFS", 0);
		
		new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(Splash.this,LoginActivity.class);
                
                String login = prefs.getString("login", "");
        		String senha = prefs.getString("senha", "");
        		
        		if (!login.equals("") && !senha.equals("")) {
        			
        			String json = prefs.getString("json", "");
        			
        			if (!json.equals("")){

        			mainIntent = new Intent(Splash.this, ListaDeDisciplinas.class);
        			mainIntent.putExtra("json", json);
        			//startActivity(it);
        			
        			}
        		}else{
        			mainIntent = new Intent(Splash.this,LoginActivity.class);
        		}
                
                Splash.this.startActivity(mainIntent);
                Splash.this.finish();
            }
        }, SPLASH_DISPLAY_LENGHT);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}

}
