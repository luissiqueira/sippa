package br.com.roove.sippa.model;

import java.io.Serializable;

public class Avaliacao implements Serializable{
	private String nome;
	private String nota;
	private double multiplicador;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public double getMultiplicador() {
		return multiplicador;
	}
	public void setMultiplicador(double multiplicador) {
		this.multiplicador = multiplicador;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getNome() + " - " + getNota();
	}
}
