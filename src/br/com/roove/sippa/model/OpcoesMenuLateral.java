package br.com.roove.sippa.model;

public class OpcoesMenuLateral {
	
	private int id;
	private String opcao;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOpcao() {
		return opcao;
	}
	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}

}
