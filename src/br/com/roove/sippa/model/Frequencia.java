package br.com.roove.sippa.model;

import java.io.Serializable;

public class Frequencia implements Serializable{
	private String presente;
	private String conteudo;
	private String data;
	
	
	public String getPresente() {
		return presente;
	}
	public void setPresente(String presente) {
		this.presente = presente;
	}
	public String getConteudo() {
		return conteudo;
	}
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
