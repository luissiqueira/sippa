package br.com.roove.sippa.model;

import java.io.Serializable;

public class Noticia implements Serializable{
	
	private String noticia;
	private String data;
	
	public String getNoticia() {
		return noticia;
	}
	public void setNoticia(String noticia) {
		this.noticia = noticia;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getData() + " - " + getNoticia();
	}
	
	
	
}
