package br.com.roove.sippa.model;

import java.io.Serializable;

public class Arquivo implements Serializable{
	private String data;
	private String nome;
	private String URL;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getData() + " - " + getNome();
	}
}
