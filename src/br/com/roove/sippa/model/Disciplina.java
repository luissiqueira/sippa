package br.com.roove.sippa.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Disciplina implements Serializable{
	
	private String nome;
	private String codigo;
	private String frequencia_resumo;
	private String professor;
	private String porcentagem_frequencia;
	private String emailProfessor;
	private ArrayList<Noticia> noticias;
	private ArrayList<Avaliacao> avaliacoes;
	private ArrayList<Frequencia> frequencia;
	private ArrayList<Arquivo> arquivos;
	private String media;
	
	
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getPorcentagem_frequencia() {
		return porcentagem_frequencia;
	}

	public void setPorcentagem_frequencia(String porcentagem_frequencia) {
		this.porcentagem_frequencia = porcentagem_frequencia;
	}

	public ArrayList<Arquivo> getArquivos() {
		return arquivos;
	}

	public void setArquivos(ArrayList<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	

	public String getFrequencia_resumo() {
		return frequencia_resumo;
	}

	public void setFrequencia_resumo(String frequencia_resumo) {
		this.frequencia_resumo = frequencia_resumo;
	}

	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	public String getEmailProfessor() {
		return emailProfessor;
	}

	public void setEmailProfessor(String emailProfessor) {
		this.emailProfessor = emailProfessor;
	}

	public ArrayList<Noticia> getNoticias() {
		return noticias;
	}

	public void setNoticias(ArrayList<Noticia> noticias) {
		this.noticias = noticias;
	}

	public ArrayList<Avaliacao> getAvaliacoes() {
		return avaliacoes;
	}

	public void setAvaliacoes(ArrayList<Avaliacao> avaliacoes) {
		this.avaliacoes = avaliacoes;
	}

	public ArrayList<Frequencia> getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(ArrayList<Frequencia> frequencia) {
		this.frequencia = frequencia;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}
	
}
